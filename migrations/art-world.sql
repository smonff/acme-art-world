PRAGMA foreign_keys = ON;
-- 1 up
CREATE TABLE Agent(
       id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       name       TEXT    NOT NULL,
       reputation INTEGER NOT NULL
);
CREATE TABLE Place(
       id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       name TEXT    NOT NULL,
       kind TEXT
);
CREATE TABLE Event (
       id       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       datetime DATETIME NOT NULL,
       place_id INTEGER,
       title    TEXT,
       FOREIGN KEY (place_id) REFERENCES Place (id)
);
-- 1 down
DROP TABLE Agent;
DROP TABLE Event;
DROP TABLE Place;

-- 2 up
ALTER TABLE Agent ADD COLUMN kind TEXT;
-- 2 down
CREATE TABLE Agent_backup(
       id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       name       TEXT    NOT NULL,
       reputation INTEGER NOT NULL
);
INSERT INTO Agent_backup SELECT id, name, reputation FROM Agent;
DROP TABLE Agent;
ALTER TABLE Agent_backup RENAME TO Agent;

-- 3 up
INSERT INTO Agent( name, reputation, kind ) VALUES ('Vincent Tremblay',100000, 'curator');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Hortense Green',100000, 'artist');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Jimmy Wolf',100000, 'colector');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Marianna Bauch',100000, 'gallerist');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Vladimir Wolf',6660, 'gallerist');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Maegan Stark',7770, 'gallerist');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Adelle Jerde',7770, 'gallerist');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Sibyl Larkin',5550, 'gallerist');
INSERT INTO Agent( name, reputation, kind ) VALUES ('Rylan Bergstrom',6660, 'gallerist');
INSERT INTO Agent( name, reputation ) VALUES ('Marcos Roob',777);
INSERT INTO Agent( name, reputation ) VALUES ('Berry Willms',888);
INSERT INTO Agent( name, reputation ) VALUES ('Leonora Auer',888);
INSERT INTO Agent( name, reputation ) VALUES ('Molly Kshlerin',888);
INSERT INTO Agent( name, reputation ) VALUES ('Kailyn Osinski',888);
INSERT INTO Agent( name, reputation ) VALUES ('Kobe Hahn',888);
INSERT INTO Agent( name, reputation ) VALUES ('Heather Koss',888);
INSERT INTO Agent( name, reputation ) VALUES ('Finn Thompson',888);
INSERT INTO Agent( name, reputation ) VALUES ('Bridie Graham',888);
INSERT INTO Agent( name, reputation ) VALUES ('Arnold Gulgowski',888);
INSERT INTO Agent( name, reputation ) VALUES ('Matilda Emmerich',888);
INSERT INTO Agent( name, reputation ) VALUES ('Destany Willms',888);
INSERT INTO Agent( name, reputation ) VALUES ('Adrienne Bogan',888);
INSERT INTO Agent( name, reputation ) VALUES ('Krista Herzog',888);
-- 3 down
DELETE FROM Agent;

-- 4 up
ALTER TABLE Place ADD COLUMN latitude   REAL;
ALTER TABLE Place ADD COLUMN longitude  REAL;
ALTER TABLE Place ADD COLUMN money      INTEGER;
ALTER TABLE Place ADD COLUMN reputation INTEGER;
-- 4 down
CREATE TABLE Place_backup(
       id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       name TEXT    NOT NULL,
       kind TEXT
);
INSERT INTO Place_backup SELECT id, name, kind FROM Place;
DROP TABLE Place;
ALTER TABLE Place_backup RENAME TO Place;

-- 5 up
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4954, -46.85635, 'workshop', NULL, 'Vincent''s hole' , 123 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.49481, -46.85426, 'workshop', NULL, 'Hortense''s Workshop', 710 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.496, -46.8589, 'workshop', NULL, 'Jimmy''s Workshop', 524 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4877, -46.8439, 'gallery', 100000, 'Tenis Clube Gallery', 7000 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.48568, -46.86937, 'museum', 1000000, 'Green Valley Museum of Art', 15000 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4777, -46.8764, 'gallery', NULL, 'Marianna Bauch Gallery', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.473, -46.8731, 'squat', NULL, 'The Refactory', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.5128, -46.8361, 'squat', 800, 'The Toilet Factory', 700 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4781, -46.8605, 'workshop', NULL, 'Rylan''s Corner', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.47739, -46.86159, 'gallery', NULL, 'Vladimir Wolf Gallery', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.47727, -46.86182, 'gallery', NULL, 'Maegan Stark Gallery', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.47715, -46.86201, 'gallery', NULL, 'Adelle Jerde Gallery', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.47684, -46.8625, 'gallery', NULL, 'Sibyl Larkin Gallery', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.47667, -46.86273, 'gallery', NULL, 'Rylan Bergstrom Gallery', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4874, -46.8308, 'school', 20000, 'Contemporary Art School in the Cloud', 75000 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4838, -46.8213, 'museum', NULL, 'MOMDAC', NULL );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.48687, -46.82485, 'workshop', 2000, 'Foo', 7000 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4787, -46.86169, 'workshop', 5000, 'Bar', 11000 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES ( -23.4845, -46.87053, 'workshop', 500, 'Baz', 10 );
INSERT INTO Place( latitude, longitude, kind, money, name, reputation ) VALUES (  -23.48465, -46.87025, 'workshop', 100, 'Boo', 7);

-- 5 down
DELETE FROM Place;

-- 7 up
INSERT INTO Event ( datetime, place_id, title ) VALUES ( DateTime('now'), 4, 'A hot opening' );
INSERT INTO Event ( datetime, place_id, title ) VALUES ( DateTime('now'), 8, 'A concert' );

-- 7 down
DELETE FROM Event;
