#!/usr/bin/env perl
use strict;
use warnings;
use v5.20;
use Mojo::SQLite;
use Mojo::SQLite::Migrations;
use Getopt::Long;

my $version;
GetOptions( "version=i" => \$version )
  or die "Error in command line arguments";

my $sql = Mojo::SQLite->new(
  'sqlite:' .
  Mojo::File::curfile
  ->dirname
  ->sibling( 'art_test.db' )
  ->to_string );

my $migrations = Mojo::SQLite::Migrations->new( sqlite => $sql );

my $migration_file = Mojo::File::curfile
  ->dirname
  ->sibling( 'migrations', 'art-world.sql' )
  ->to_string;

$migrations->from_file( $migration_file )->migrate( $version || $migrations->latest );

my $asked_version = $version || $migrations->latest;

say 'Migration to v.' . $asked_version;
