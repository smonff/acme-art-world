package Acme::Art::World {

use 5.006;
use strict;
use warnings;

use Zydeco;


=head1 NAME

Acme::Art::World - The great new Acme::Art::World!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Acme::Art::World;

    my $foo = Acme::Art::World->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head1 AUTHOR

Sébastien Feugère, C<< <sebastien at feugere.net> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-acme-art-world at rt.cpan.org>, or through
the web interface at L<https://rt.cpan.org/NoAuth/ReportBug.html?Queue=Acme-Art-World>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Acme::Art::World


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=Acme-Art-World>

=item * CPAN Ratings

L<https://cpanratings.perl.org/d/Acme-Art-World>

=item * Search CPAN

L<https://metacpan.org/release/Acme-Art-World>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2021 by Sébastien Feugère.

This is free software, licensed under:

  The Artistic License 2.0 (GPL Compatible)


=cut

1; # End of Acme::Art::World
