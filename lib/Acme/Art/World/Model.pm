package Acme::Art::World::Model {

  use Mojo::Base -base;
  use namespace::autoclean;
  use Types::Standard qw( Str );
  use Carp;
  use feature qw( postderef signatures );
  no warnings qw( experimental::postderef experimental::signatures );

  use DDP;

  has sqlite => sub ( $self ) { Carp::croak 'sqlite is required'};

  # https://github.com/Grinnz/Mojo-SQLite/blob/master/examples/blog/lib/Blog.pm
  # helper 'dbh' => sub ( $c ) {}

  sub agent ( $self, $id ) {
    p $id;
    p $self->sqlite->db->select('Agent', undef, { id => $id })->hash;
    return $self->sqlite->db->select('Agent', undef, { id => $id })->hash;
  }

  sub agents ( $self ) {
    return $self->sqlite->db->select('Agent')->hashes;
  }

  sub places ( $self ) {
    return $self->sqlite->db->select('Place')->hashes;
  }

  #has dbh ( type => InstanceOf[ 'Art::World::Model' ], builder => true, lazy => true );
  #has db  ( type => HashRef[ Str ], builder => true, lazy => true );

  # sub insert ( Str $table, HashRef $attributes ) {
  #   # TODO IT MUST BE TESTED
  #   unless ( $self->does( 'Art::World::Unserializable' )) {
  #     try {
  #       my $row = $self->dbh->insert( ucfirst lc $table, $attributes);
  #     } catch {
  #       cluck 'You tried to insert to ' . $table . ' but this table doesn\'t exist';
  #     }
  #   }
  # }

  sub _build_dbh {

    # my $dbh = Teng::Schema::Loader->load(
    #   dbh       => DBI->connect(
    #     'dbi:' . $self->db->{ kind } .
    #     ':dbname=' . $self->db->{ name },
    #     '', '' ),
    #   namespace => __PACKAGE__ . '::Model'
    #  );
    # return $dbh;
  }

  sub _build_db {
    # return {
    #   name => $self->config->{ DB }->{ NAME },
    #   kind => $self->config->{ DB }->{ KIND },
    # };
  }
}


=head1 Crud (OBSOLETE)

The C<Crud> role makes possible to serialize most of the entities. For this to be
possible, they need to have a corresponding table in the database, plus they
need to not have a C<unserializable?> tag attribute. A Zydeco tag role is a role
without behavior that can only be checked using C<does>.

The Crud role is made possible thanks to L<Teng>, a simple DBI wrapper and O/R
Mapper. C<Teng> is used through it's L<schema loader|Teng::Schema::Loader> that
directly instanciate an object schema from the database.

When trying to update a value, it is necessary to pass a table name and a
hashref of attributes corresponding to the columns values. The table name is not case
sensitive when using C<Art::World> C<Crud> helpers.

  my $row = $self->dbh
    ->insert( 'agent', {
      name => $artist->name,
      reputation => $artist->reputation });

Note that it is extremely important that the testing of the database should be
done in the C<t/crud.t> file. If you want to test database or model
functionnalities in other tests, remind to create objects by specifiying the
config parameter that use the C<test.conf> file: only the database referenced in
C<test.conf> will be available on cpantesters and C<art.db> wont be available there,
only C<t/test.db> is available.

=cut

1;
