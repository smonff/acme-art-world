requires 'Art::World', '== 0.19_01';
requires 'Data::Printer', '== 1.000004';
requires 'Faker', '== 1.04';
requires 'Future::AsyncAwait', '== 0.50';
requires 'Mojo::SQLite', '== 3.005';
requires 'Mojolicious', '== 9.17';
requires 'Mojolicious::Plugin::PODViewer', '== 0.006';
requires 'SQL::Abstract', '== 2.000001';
