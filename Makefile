CARTONEXE=carton exec
DB_DEBUG=DBI_TRACE=1 MOJO_MIGRATIONS_DEBUG=1

run:
	carton exec -- morbo -v mini-world

test-crud:
	${CARTONEXE} -- prove -vl t/20_crud.t

db-schema:
	${CARTONEXE} script/teng_schema_dumper.pl > lib/Art/World/Model/Schema.pm

# Combining db-dump and db-recreate, by editing the SQL file in between makes possible to modify the schema
db-dump:
	echo '.dump' | sqlite3 art.db > schema.sql
	rm art.db

db-recreate:
	cat schema.sql | sqlite3 art.db

migration:
	@${DB_DEBUG} ${CARTONEXE} -- migrations/art-world.pl

# You can edit the version to migrate to
migration-version:
	#@${DB_DEBUG}
	@${CARTONEXE} -- migrations/art-world.pl -v 5
