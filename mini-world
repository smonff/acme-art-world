#!/usr/bin/env perl
use 5.24.0;
use Mojolicious::Lite -signatures;
use Mojo::JSON qw(encode_json);
use Mojo::File qw(path);
use Mojo::SQLite;
use lib 'lib';

use DDP;

use Art::World;
use Acme::Art::World::Model;

plugin 'PODViewer';

helper 'sqlite' => sub ( $self ) {
   # determine the storage location
  my $file = $self->app->config->{database} || 'art.db';
  unless ($file =~ /^:/) {
    $file = Mojo::File->new( $file );
    unless ($file->is_abs) {
      $file = $self->app->home->child( "$file" );
    }
  }

  my $sqlite = Mojo::SQLite->new
    ->from_filename("$file")
    ->auto_migrate(1);

  # attach migrations file
  $sqlite->migrations->from_file(
    $self->app->home->child('migrations/art-world.sql')
   )->name('artworld');

  return $sqlite;
};

helper 'model' => sub ( $self ) {
  p $self->app->sqlite;
  return Acme::Art::World::Model->new(
    sqlite => $self->app->sqlite );
};


helper 'metadata' => sub ( $c, $meta ) {
  return {
    name    => $meta->name,
    version => Art::World->VERSION,
  }
};

helper 'rand_int' => sub ( $c, $range ) {
  return 1 + int rand( $range );
};

helper 'sketch' => sub ( $c ) {
  my $images = Mojo::File->new('./public/entities/')->list_tree;
  my @cleaned_paths = map { $_ =~ s/^public//r } map { $_->to_string } $images->@*;
  return @cleaned_paths[ $c->rand_int( scalar @cleaned_paths - 1 ) ];
};

my $world = Art::World->new_playground(
  #config => Config::Tiny->read( 'art.conf' ),
  name   => 'Art Sharks City' );
my $museum = Art::World->new_museum(
  name => 'SCMA'
 );

get '/' => sub ( $self ) {
  $self->render( template => 'index' );
};

get '/agent/:id' => sub ( $self ) {
  $self->render(template => 'agent', title => 'agent');
};

get '/api' => sub ( $self ) {
  $self->render( json => {
    'world' => {
      agents  => $self->model->agents,
      places  => $self->model->places,
      $self->metadata( $world )->%*,
    }});
};

get '/api/agent/:id' => sub ( $self ) {
  $self->render( json => {
    'world' => {
      agent  => $self->model->agent( $self->param( 'id' )),
      $self->metadata( $world )->%*,
    }});
};

app->start;
